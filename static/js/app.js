(function () {
    'use strict';

    angular
        .module('rms', [])
        .run(run)
        .config(function ($interpolateProvider, $locationProvider, $httpProvider) {
            $locationProvider.html5Mode(true);
            $interpolateProvider.startSymbol('{[{');
            $interpolateProvider.endSymbol('}]}');
            $httpProvider.defaults.xsrfCookieName = 'csrftoken';
            $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
        });

    function run($http) {
        $http.defaults.xsrfHeaderName = 'X-CSRFToken';
        $http.defaults.xsrfCookieName = 'csrftoken';
    }
})();